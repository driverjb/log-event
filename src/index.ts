import { Settings as LogSettings } from './settings/Settings.singleton';
export { SettingsSchema, SettingsJson } from './settings/Settings.singleton';
export * as Event from './event';
export { Level } from './settings/Level.module';

export const Settings = LogSettings.get();
