/**
 * Options:
 *
 *   - local: Local time (EST)
 *   - utc: Coordinated Universal Time
 *
 * All times will be presented in ISO format
 */
export type TimeMode = 'local' | 'utc';
/** Array of options typically used for input validation */
export const timeModes = ['local', 'utc'];
