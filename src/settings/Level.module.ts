/** Log levels */
export type Level = 'trace' | 'debug' | 'info' | 'notice' | 'warn' | 'error' | 'fatal';

/**
 * A map containing the numerical value of each log level setting. These are used to compare to the
 * configured level to determine if an event will be printed or not.
 */
export const mapLevelValue = new Map<Level, number>([
  ['trace', 1],
  ['debug', 2],
  ['info', 3],
  ['notice', 4],
  ['warn', 5],
  ['error', 6],
  ['fatal', 7]
]);

/** Array of options typically used for input validation */
export const levels: Level[] = ['trace', 'debug', 'info', 'notice', 'warn', 'error', 'fatal'];
