import { Level, levels } from './Level.module';
import { TimeMode, timeModes } from './TimeMode.module';
import { OutputFormat, outputFormats } from './OutputFormat.module';
import Joi from 'joi';

interface SettingsJsonUpdate {
  appName?: string;
  color?: boolean;
  enabled?: boolean;
  level?: Level;
  outputFormat?: OutputFormat;
  timeMode?: TimeMode;
  unknownOperator?: string;
}

export interface SettingsJson {
  appName: string;
  color: boolean;
  enabled: boolean;
  level: Level;
  outputFormat: OutputFormat;
  timeMode: TimeMode;
  unknownOperator: string;
}

/**
 * Schema that provides the default configuration as well as validation logic for when custom
 * settings are provided
 */
const Schema = Joi.object<SettingsJsonUpdate>({
  appName: Joi.string(),
  color: Joi.boolean(),
  enabled: Joi.boolean(),
  level: Joi.string().valid(...levels),
  outputFormat: Joi.string().valid(...outputFormats),
  timeMode: Joi.string().valid(...timeModes),
  unknownOperator: Joi.string()
});

export const SettingsSchema = Joi.object<SettingsJson>({
  appName: Joi.string().default('MyApp'),
  color: Joi.boolean().default(false),
  enabled: Joi.boolean().default(true),
  level: Joi.string()
    .valid(...levels)
    .default('info'),
  outputFormat: Joi.string()
    .valid(...outputFormats)
    .default('standard'),
  timeMode: Joi.string()
    .valid(...timeModes)
    .default('local'),
  unknownOperator: Joi.string().default('ANONYMOUS')
});

/**
 * The class that controls the behavior of all derived events and printers
 */
export class Settings {
  private static instance: Settings | null = null;
  private data: SettingsJson;
  private constructor() {
    this.data = {
      appName: 'MyApp',
      color: false,
      enabled: true,
      level: 'info',
      outputFormat: 'standard',
      timeMode: 'local',
      unknownOperator: 'ANONYMOUS'
    };
  }
  /**
   * The name of the application presented in log events
   */
  public get AppName() {
    return this.data.appName;
  }
  public get Color() {
    return this.data.color;
  }
  /**
   * Should the printers write logs at all
   */
  public get Enabled() {
    return this.data.enabled;
  }
  /**
   * The minimum required level of an event before it will be printed
   */
  public get Level() {
    return this.data.level;
  }
  /**
   * The format of printer output
   */
  public get OutputFormat() {
    return this.data.outputFormat;
  }
  /**
   * The style of timestamp that will be generated
   */
  public get TimeMode() {
    return this.data.timeMode;
  }
  /**
   * The operator name to be used if no operator is provided
   */
  public get UnknownOperator() {
    return this.data.unknownOperator;
  }
  /**
   * Update the existing settings
   * @param options
   */
  public update(options: SettingsJsonUpdate) {
    const { value, error } = Schema.validate(options);
    if (error) throw new Error(error.message);
    this.data = Object.assign(this.data, value);
  }
  /**
   * Get a singleton instance of Settings
   * @returns
   */
  public static get(): Settings {
    if (!Settings.instance) Settings.instance = new Settings();
    return Settings.instance;
  }
}
