/**
 * Options:
 *
 *   -s tandard - Log will be printed in human friendly format
 *   - json - Log will be printed in json format
 *
 * Both formats are directly consumable by Splunk as is
 */
export type OutputFormat = 'standard' | 'json';
/** Array of options typically used for input validation */
export const outputFormats: OutputFormat[] = ['standard', 'json'];
