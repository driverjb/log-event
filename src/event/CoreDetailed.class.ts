import { Core, LogData, FullOptions } from './Core.class';

export abstract class CoreDetailed extends Core {
  constructor(message: string, options: FullOptions = {}) {
    super(message, options);
  }
  public abstract generateDataBlock(): LogData;
  /**
   * Convert a bunch of data into a legible log format
   * @param data Data package to format
   * @param uuid Unique identifier for requests to include in data
   * @param error Error is available to include
   * @returns The provided data as a string
   */
  private prepareData(d: { [key: string]: any }) {
    let o = '';
    Object.keys(d)
      .filter((k) => d[k] !== undefined)
      .sort()
      .forEach(
        (k) => (o += o === '' ? `${k}=${JSON.stringify(d[k])}` : ` ${k}=${JSON.stringify(d[k])}`)
      );
    return `[${o}]`;
  }
  public override toJson() {
    const temp = Object.assign(super.toJson(), this.generateDataBlock());
    const s: LogData = {};
    const sorted = Object.keys(temp)
      .sort()
      .reduce((o, k) => {
        o[k] = temp[k];
        return o;
      }, s);
    return sorted;
  }
  public override toString(): string {
    return super.toString().replace(/\$data/, this.prepareData(this.generateDataBlock()));
  }
}
