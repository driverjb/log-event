import { Level } from '../settings/Level.module';
import { Settings } from '../settings/Settings.singleton';
import clc from 'cli-color';

const settings = Settings.get();

export function Timestamp(text: string) {
  return settings.Color ? clc.yellowBright(text) : text;
}
export function AppName(text: string) {
  return settings.Color ? clc.blueBright(text) : text;
}
export function Uuid(text: string) {
  return settings.Color ? clc.white(text) : text;
}

export function Level(level: Level) {
  if (!settings.Color) return level.toUpperCase();
  const l = level.toUpperCase();
  switch (level) {
    case 'trace':
      return clc.blueBright(l);
    case 'debug':
      return clc.cyanBright(l);
    case 'info':
      return clc.green(l);
    case 'notice':
      return clc.greenBright(l);
    case 'warn':
      return clc.yellowBright(l);
    case 'error':
      return clc.redBright(l);
    case 'fatal':
      return clc.magentaBright(l);
  }
}
export function Operator(text: string) {
  return settings.Color ? clc.white(text.toUpperCase()) : text.toUpperCase();
}
export function Namespace(text: string) {
  return settings.Color ? clc.blueBright(text) : text;
}
export function Message(text: string) {
  return settings.Color ? clc.yellowBright(text) : text;
}
