import { Level } from '../settings/Level.module';
import { Settings } from '../settings/Settings.singleton';
import { v4 as uuidV4 } from 'uuid';
import { getTimestamp } from '../util/getTimestamp.function';
import assert from 'assert';
import { Print } from '../util/print.function';
import * as Color from './Color.module';

const settings = Settings.get();

const uuidV4Reg = /^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i;

const standardString = '$timestamp $appName $uuid $level $operator $namespace $data $message';

export type LogDataValue = string | null | undefined | number | object | boolean;

export interface LogData {
  [key: string]: LogDataValue;
}

export interface CoreOptions {
  level?: Level;
  namespace?: string;
  uuid?: string;
}

export interface OperatorOption {
  operator?: string;
}

export type FullOptions = CoreOptions & OperatorOption;

export class Core {
  private level: Level;
  private namespace: string;
  private operator: string;
  private timestamp: string;
  private uuid: string;
  private printed: boolean;
  constructor(private message: string, options: FullOptions = {}) {
    this.printed = false;
    this.level = options.level ?? 'info';
    this.namespace = options.namespace ?? 'core';
    this.operator = options.operator ?? settings.UnknownOperator;
    this.timestamp = getTimestamp();
    this.uuid = options.uuid ?? uuidV4();
    assert.equal(uuidV4Reg.test(this.uuid), true, 'Invalid uuid version 4 provided');
  }
  public get Level() {
    return this.level;
  }
  public get HasPrinted() {
    return this.printed;
  }
  public get Namespace() {
    return this.namespace;
  }
  public get Operator() {
    return this.operator;
  }
  public get Timestamp() {
    return this.timestamp;
  }
  public get UUID() {
    return this.uuid;
  }
  public Print() {
    Print(this);
    this.printed = true;
    return this;
  }
  public toJson(): LogData {
    return {
      timestamp: this.timestamp,
      appName: settings.AppName,
      uuid: this.uuid,
      level: this.level,
      operator: this.operator,
      namespace: this.namespace,
      message: this.message
    };
  }
  public toString() {
    return standardString
      .replace(/\$timestamp/, Color.Timestamp(this.timestamp))
      .replace(/\$appName/, Color.AppName(settings.AppName))
      .replace(/\$uuid/, Color.Uuid(this.uuid))
      .replace(/\$level/, Color.Level(this.level))
      .replace(/\$operator/, Color.Operator(this.operator))
      .replace(/\$namespace/, Color.Namespace(this.namespace))
      .replace(/\$message/, Color.Message(this.message));
  }
}
