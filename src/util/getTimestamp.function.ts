import { Settings } from '../settings/Settings.singleton';
import { DateTime } from 'luxon';

const settings = Settings.get();

export function getTimestamp(): string {
  const now = DateTime.now();
  if (settings.TimeMode === 'local') return now.toISO();
  else return now.toUTC().toISO();
}
