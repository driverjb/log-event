import { Settings } from '../settings/Settings.singleton';
import { mapLevelValue, Level } from '../settings/Level.module';
import { Core } from '../event/Core.class';

const settings = Settings.get();

function shouldPrint(level: Level) {
  if (!settings.Enabled) return false;
  const testLevel = mapLevelValue.get(level) ?? 2; //default to info
  const currrentLevel = mapLevelValue.get(settings.Level) ?? 2; //default to info
  return testLevel >= currrentLevel;
}

export function Print(e: Core) {
  if (!shouldPrint(e.Level)) return false;
  if (settings.OutputFormat == 'standard') console.log(e.toString().replace(/\$data/, '[]'));
  else console.log(JSON.stringify(e.toJson()));
  return true;
}
